# IFrame Super
#### Android IFrame Super

[![](https://www.jitpack.io/v/com.gitee.kframe/super.svg)](https://www.jitpack.io/#com.gitee.kframe/super)

> Activity和Fragment的引用，方便框架切换

#### 使用方式

##### Step 1. Add the JitPack repository to your build file

~~~
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
~~~

##### Step 2. Add the dependency

~~~
dependencies {
    //implementation deps.kframe.super_base     //三选一
    //implementation deps.kframe.super_support  //三选一
    //implementation deps.kframe.super_androidx //三选一
}

~~~

##### 如果有引用包冲突和不必要的包，请排除

